/*
** my_lib_c.h for my_lib_c in /home/le-mai_s/librairie/librairie_C/my_libc
**
** Made by sebastien le-maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Thu Aug 13 08:56:39 2015 sebastien le-maire
** Last update Thu Nov  5 19:14:45 2015 sebastien le-maire
*/

#ifndef MY_LIB_C_H_
# define MY_LIB_C_H_

# include <stdarg.h>
# include "myc.h"

# define NB_FLAGS	(6)

typedef	struct	s_flag
{
  char		c;
  void		(*fct)(va_list *ap);
}		t_flag;

typedef	struct	s_fd_flag
{
  char		c;
  void		(*fct)(const int fd, va_list *ap);
}		t_fd_flag;

void		my_printf(const char *format, ...);
void		my_fprintf(const int fd, const char *format, ...);
void		*fill_struct(void *structure, void *(fill_data)(),
			     const unsigned int size, ...);

/*
**#########    Flags functions my_printf   ##########
*/
void		flag_char(va_list *ap);
void		flag_string(va_list *ap);
void		flag_entier(va_list *ap);
void		flag_hexa(va_list *ap);
void		flag_binaire(va_list *ap);

/*
**#########    Flags functions my_printf   ##########
*/
void		fd_flag_char(const int fd, va_list *ap);
void		fd_flag_string(const int fd, va_list *ap);
void		fd_flag_entier(const int fd, va_list *ap);
void		fd_flag_hexa(const int fd, va_list *ap);
void		fd_flag_binaire(const int fd, va_list *ap);

#endif /* !MY_LIB_C_H_ */
