/*
** myc.h for my_lib_c in /home/le-mai_s/librairie/librairie_C/my_libc
**
** Made by sebastien le-maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Thu Aug 13 08:56:46 2015 sebastien le-maire
** Last update Wed Oct 21 10:27:02 2015 sebastien le-maire
*/

#ifndef MYC_H_
# define MYC_H_

# define INT_TO_CHAR(nb)	((nb) + '0')
# define CHAR_TO_INT(nb)	((nb) - '0')
# define IS_NUM(nb)		((nb) >= '0' && (nb) <= '9')
# define NOT_NUM(nb)		((nb) < '0' || (nb) > '9')
# define ABS(nb)		(((nb) < 0) ? -(nb) : (nb))

typedef	enum	e_bool
  {
		FALSE,
		TRUE
  }		t_bool;

/*
** Messages errors
*/
# define MSG_ERROR_NOT_NUMBER	"Error: isn't number !\n"

/*
**  ##########   Data of Bases   ##########
*/
# define BINAIRE		"01"
# define DECIMAL		"0123456789"
# define HEXADECIMAL		"0123456789ABCDEF"

/*
**  ##########   Data of Get_next_line   ##########
*/
# define BUFF_SIZE (100)

typedef	struct	s_gnl
{
  char		buf[BUFF_SIZE + 1];
  char		*tmp;
  char		*line;
}		t_gnl;

/*
** ##########   Display functions   ##########
*/
void		my_putchar(const char c);
void		my_putstr(const char *str);
int		my_puterror(const char *str, const int num_error);
void		my_putnbr(int nb);
void		my_putnbr_base(int nbr, const char *base);
void		show_strtab(const char **tab, const char *separator);

/*
** ##########   Gets data functions   ##########
*/
unsigned int	my_strlen(const char *str);
int		my_atoi(const char *str);
int		my_atoi_base(const char *str, const char *base);
int		my_getnbr(const char *str, t_bool count_sign);
int		parse_atoi(const char *str, t_bool *err);
char		*convert_base(const char *nbr, const char *base_from,
			      const char *base_to);

/*
** ##########   String manipulate functions   ##########
*/
char		*my_strcpy(char *dest, const char *src);
char		*my_strncpy(char *dest, const char *src,
			    const unsigned int size);
char		*my_strcat(char *dest, const char *src);
char		*my_strncat(char *dest, const char *src,
			    const unsigned int size);
int		my_strcmp(const char *s1, const char *s2);
int		my_strncmp(const char *s1, const char *s2,
			   const unsigned int size);
char		*my_strdup(const char *src);
char		*my_strndup(const char *src, const unsigned int size);
char		*my_strdupcat(char *s1, const char *s2);
char		*newstr(const unsigned int size);
char		**my_str_to_wordtab(const char *str, const char *delimit);

/*
** ##########   memory manipulate functions   ##########
*/
void		*my_memset(void *mem, const unsigned int size);
void		destroy_strtab(char **tab);

/*
** #########    File Descriptor functions   ##########
*/
char		*get_next_line(const int fd);
void		my_fputc(const int fd, const char c);
void		my_fputs(const int fd, const char *str);
void		my_fputnbr(const int fd, int nb);
void		my_fputnbr_base(const int fd, int nbr, const char *base);

#endif /* !MYC_H_ */
