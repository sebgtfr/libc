##
## Makefile for my_lib_c in /home/le-mai_s/librairie/librairie_C/my_libc
## 
## Made by sebastien le-maire
## Login   <le-mai_s@epitech.net>
## 
## Started on  Thu Aug 13 09:10:28 2015 sebastien le-maire
## Last update Sun May 15 20:08:12 2016 Sébastien Le Maire
##

ifeq ($(DEBUG), yes)

CFLAGS		+= -g -g3 -ggdb

endif

PATH_LIB	= ./
PATH_LIB_SRCS	= $(PATH_LIB)srcs/

NAME_LIB_MYC	= $(PATH_LIB)libmyc.a

CFLAGS		+= -Wall -Wextra -Werror
CFLAGS		+= -I./include

SRCS_LIB_MYC	= $(PATH_LIB_SRCS)my_putchar.c
SRCS_LIB_MYC	+= $(PATH_LIB_SRCS)my_putstr.c
SRCS_LIB_MYC	+= $(PATH_LIB_SRCS)my_puterror.c
SRCS_LIB_MYC	+= $(PATH_LIB_SRCS)my_putnbr.c
SRCS_LIB_MYC	+= $(PATH_LIB_SRCS)my_putnbr_base.c
SRCS_LIB_MYC	+= $(PATH_LIB_SRCS)convert_base.c
SRCS_LIB_MYC	+= $(PATH_LIB_SRCS)my_strlen.c
SRCS_LIB_MYC	+= $(PATH_LIB_SRCS)my_atoi.c
SRCS_LIB_MYC	+= $(PATH_LIB_SRCS)my_atoi_base.c
SRCS_LIB_MYC	+= $(PATH_LIB_SRCS)my_getnbr.c
SRCS_LIB_MYC	+= $(PATH_LIB_SRCS)parse_getnbr.c
SRCS_LIB_MYC	+= $(PATH_LIB_SRCS)my_strcpy.c
SRCS_LIB_MYC	+= $(PATH_LIB_SRCS)my_strncpy.c
SRCS_LIB_MYC	+= $(PATH_LIB_SRCS)my_strcat.c
SRCS_LIB_MYC	+= $(PATH_LIB_SRCS)my_strncat.c
SRCS_LIB_MYC	+= $(PATH_LIB_SRCS)my_strcmp.c
SRCS_LIB_MYC	+= $(PATH_LIB_SRCS)my_strncmp.c
SRCS_LIB_MYC	+= $(PATH_LIB_SRCS)my_strdup.c
SRCS_LIB_MYC	+= $(PATH_LIB_SRCS)my_strndup.c
SRCS_LIB_MYC	+= $(PATH_LIB_SRCS)newstr.c
SRCS_LIB_MYC	+= $(PATH_LIB_SRCS)my_str_to_wordtab.c
SRCS_LIB_MYC	+= $(PATH_LIB_SRCS)show_strtab.c
SRCS_LIB_MYC	+= $(PATH_LIB_SRCS)destroy_strtab.c
SRCS_LIB_MYC	+= $(PATH_LIB_SRCS)my_memset.c
SRCS_LIB_MYC	+= $(PATH_LIB_SRCS)get_next_line.c
SRCS_LIB_MYC	+= $(PATH_LIB_SRCS)fct_write_fd.c
SRCS_LIB_MYC	+= $(PATH_LIB_SRCS)flags_basic.c
SRCS_LIB_MYC	+= $(PATH_LIB_SRCS)fbasic_flags.c
SRCS_LIB_MYC	+= $(PATH_LIB_SRCS)my_printf.c
SRCS_LIB_MYC	+= $(PATH_LIB_SRCS)my_fprintf.c
SRCS_LIB_MYC	+= $(PATH_LIB_SRCS)fill_struct.c

OBJS_LIB_MYC	= $(SRCS_LIB_MYC:.c=.o)

AR		= ar rc

RAN		= ranlib

RM		= rm -fr

###################### DECO #######################

.c.o:
	@$(CC) $(CFLAGS) -c $< -o $@
	@echo -e "\t\t\033[01;32m[COMPILE]\033[00m \t=> $<"

##################################################

all: $(NAME_LIB_MYC)

$(NAME_LIB_MYC): print_compile_lib $(OBJS_LIB_MYC)
	@echo -e "\033[01;33mCREATE LIBRARY\033[00m"
	@$(AR) $(NAME_LIB_MYC) $(OBJS_LIB_MYC)
	@echo -e "\t\t\033[01;33m[AR]\033[00m \t\t=> $(NAME_LIB_MYC)"
	@$(RAN) $(NAME_LIB_MYC)
	@echo -e "\t\t\033[01;33m[RAN]\033[00m \t\t=> $(NAME_LIB_MYC)"

clean:
	@echo -e "\033[01;31mDELETE OBJECTS\033[00m"
	@$(RM) $(OBJS_LIB_MYC)

fclean: clean
	@echo -e "\033[01;31mDELETE LIBRARY\033[00m"
	@$(RM) $(NAME_LIB_MYC)

re: fclean all

print_compile_lib:
	@echo -e "\033[01;32mCOMPILE LIBRARY\033[00m"

.PHONY: all clean fclean re print_compile_lib
