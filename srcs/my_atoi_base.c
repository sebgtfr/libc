/*
** my_atoi_base.c for libc in /home/le-mai_s/librairie/librairie_C/my_libc
**
** Made by sebastien le-maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Thu Aug 20 09:55:37 2015 sebastien le-maire
** Last update Thu Aug 20 11:48:01 2015 sebastien le-maire
*/

#include "myc.h"

int		my_atoi_base(const char *nbr, const char *base)
{
  int		s;
  int		nb;
  unsigned int	pos;
  unsigned int	size_base;
  unsigned int	b;

  s = (nbr[0] == '-') ? -1 : 1;
  pos = (s == -1) ? 1 : 0;
  nb = 0;
  size_base = my_strlen(base);
  while (nbr[pos])
    {
      b = 0;
      while (b < size_base && base[b] != nbr[pos])
	++b;
      if (base[b] == nbr[pos])
	nb = (nb * size_base) + b;
      else
	return (0);
      ++pos;
    }
  return (nb * s);
}
