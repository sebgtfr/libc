/*
** my_strdupcat.c for libc in /home/le-mai_s/librairie/librairie_C/libc
**
** Made by sebastien le-maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Mon Aug 10 10:33:07 2015 sebastien le-maire
** Last update Thu Aug 13 11:23:34 2015 sebastien le-maire
*/

#include <stdlib.h>
#include "myc.h"

char		*my_strdupcat(char *s1, const char *s2)
{
  char		*tmp;

  if ((!s1[0] && !s2[0]))
    return (NULL);
  else if ((s1[0] && !s2[0]))
    return (s1);
  if ((!s1[0] && s2[0]))
    {
      if (!(tmp = my_strdup(s2)))
	return (NULL);
      free(s1);
      return (tmp);
    }
  if (!(tmp = newstr(my_strlen(s1) + my_strlen(s2) + 1)))
    return (NULL);
  tmp = my_strcpy(tmp, s1);
  tmp = my_strcat(tmp, s2);
  free(s1);
  return (tmp);
}
