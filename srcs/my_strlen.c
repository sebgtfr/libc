/*
** my_strlen.c for libc in /home/le-mai_s/librairie/librairie_C/libc
**
** Made by sebastien le-maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Mon Aug 10 10:30:52 2015 sebastien le-maire
** Last update Mon Aug 10 10:30:53 2015 sebastien le-maire
*/

#include "myc.h"

unsigned int	my_strlen(const char *str)
{
  unsigned int	i;

  i = 0;
  while (str[i])
    ++i;
  return (i);
}
