/*
** parse_atoi.c for libc in /home/le-mai_s/librairie/librairie_C/my_libc/sources
**
** Made by sebastien le-maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Fri Sep 18 11:58:08 2015 sebastien le-maire
** Last update Wed Oct 21 10:26:14 2015 sebastien le-maire
*/

#include "myc.h"

int		parse_atoi(const char *str, t_bool *err)
{
  unsigned int	i;
  int		nb;

  *err = FALSE;
  i = (str[0] == '-') ? 1 : 0;
  while (str[i] && IS_NUM(str[i]))
    ++i;
  if (!str[i])
    nb = my_atoi(str);
  else
    {
      (void)my_puterror(MSG_ERROR_NOT_NUMBER, 0);
      *err = TRUE;
    }
  return ((*err == TRUE) ? 0 : nb);
}
