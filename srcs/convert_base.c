/*
** convert_base.c for libc in /home/le-mai_s/librairie/librairie_C/my_libc
**
** Made by sebastien le-maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Thu Aug 20 10:41:03 2015 sebastien le-maire
** Last update Wed Oct 21 10:26:52 2015 sebastien le-maire
*/

#include <stdlib.h>
#include "myc.h"

static unsigned int	cb_size_nbr(int nb,
				    const unsigned int size_base,
				    int *s)
{
  unsigned int		size;

  size = 0;
  *s = (nb < 0) ? -1 : 1;
  while (nb > 0)
    {
      nb /= size_base;
      ++size;
    }
  size += ((*s) == -1) ? 1 : 0;
  return (size);
}

char		*convert_base(const char *nbr, const char *base_from,
			      const char *base_to)
{
  unsigned int	base_size;
  char		*result;
  int		nb;
  unsigned int	size;
  unsigned int	tmp_size;
  int		s;

  nb = my_atoi_base(nbr, base_from);
  base_size = my_strlen(base_to);
  size = cb_size_nbr(nb, base_size, &s);
  if (!(result = newstr(size)))
    return (NULL);
  result[0] = (s == -1) ? '-' : result[0];
  tmp_size = size;
  nb = (nb < 0) ? -nb : nb;
  while (nb > 0)
    {
      result[--tmp_size] = base_to[nb % base_size];
      nb /= base_size;
    }
  return (result);
}
