/*
** my_puterror.c for libc in /home/le-mai_s/librairie/librairie_C/libc
**
** Made by sebastien le-maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Mon Aug 10 10:30:29 2015 sebastien le-maire
** Last update Sat Oct  3 20:57:59 2015 sebastien le-maire
*/

#include <unistd.h>
#include "myc.h"

int		my_puterror(const char *str, const int num_error)
{
  (void)write(STDERR_FILENO, str, my_strlen(str));
  return (num_error);
}
