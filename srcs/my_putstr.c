/*
** my_putstr.c for libc in /home/le-mai_s/librairie/librairie_C/libc
**
** Made by sebastien le-maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Mon Aug 10 10:30:15 2015 sebastien le-maire
** Last update Sat Oct  3 20:57:53 2015 sebastien le-maire
*/

#include "myc.h"

void		my_putstr(const char *str)
{
  unsigned int	i;

  if (str)
    {
      i = 0;
      while (str[i])
	my_putchar(str[i++]);
    }
}
