/*
** my_putnbr.c for libc in /home/le-mai_s/librairie/librairie_C/libc
**
** Made by sebastien le-maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Mon Aug 10 10:30:42 2015 sebastien le-maire
** Last update Fri Sep 18 10:43:53 2015 sebastien le-maire
*/

#include "myc.h"

void		my_putnbr(int nb)
{
  if (nb < 0)
    my_putchar('-');
  if (nb >= 10 || nb <= -10)
    {
      my_putnbr(ABS(nb / 10));
      my_putnbr(ABS(nb % 10));
    }
  else
    my_putchar(INT_TO_CHAR(ABS(nb)));
}
