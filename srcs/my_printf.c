/*
** my_printf.c for libc in /home/le-mai_s/librairie/librairie_C/libc
**
** Made by sebastien le-maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Mon Aug 10 10:34:10 2015 sebastien le-maire
** Last update Wed Oct 21 10:22:16 2015 sebastien le-maire
*/

#include "my_lib_c.h"

static void    	init_flags(t_flag (*flag)[NB_FLAGS])
{
  (*flag)[0].c = 'c';
  (*flag)[0].fct = flag_char;
  (*flag)[1].c = 's';
  (*flag)[1].fct = flag_string;
  (*flag)[2].c = 'd';
  (*flag)[2].fct = flag_entier;
  (*flag)[3].c = 'i';
  (*flag)[3].fct = flag_entier;
  (*flag)[4].c = 'h';
  (*flag)[4].fct = flag_hexa;
  (*flag)[5].c = 'b';
  (*flag)[5].fct = flag_binaire;
}

static int    	check_flags(const char c, va_list *ap, t_flag *flag)
{
  unsigned int	i;

  i = 0;
  while (i < NB_FLAGS && flag[i].c != c)
    ++i;
  if (i == NB_FLAGS)
    return (1);
  flag[i].fct(ap);
  return (0);
}

void		my_printf(const char *format, ...)
{
  va_list	ap;
  t_flag	flag[NB_FLAGS];
  unsigned int	i;

  i = 0;
  init_flags(&flag);
  va_start(ap, format);
  while (format[i])
    {
      if (format[i] == '%')
	{
	  if (check_flags(format[i + 1], &ap, flag))
	    my_putchar(format[i]);
	  else
	    ++i;
	}
      else
	my_putchar(format[i]);
      ++i;
    }
  va_end(ap);
}
