/*
** my_strcmp.c for libc in /home/le-mai_s/librairie/librairie_C/libc
**
** Made by sebastien le-maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Mon Aug 10 10:32:16 2015 sebastien le-maire
** Last update Mon Aug 10 10:32:18 2015 sebastien le-maire
*/

#include "myc.h"

int		my_strcmp(const char *s1, const char *s2)
{
  unsigned int	pos;

  pos = 0;
  while ((s1[pos] || s2[pos]) && (s1[pos] == s2[pos]))
    ++pos;
  return (s1[pos] - s2[pos]);
}
