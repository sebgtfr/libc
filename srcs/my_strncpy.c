/*
** my_strncpy.c for libc in /home/le-mai_s/librairie/librairie_C/libc
**
** Made by sebastien le-maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Mon Aug 10 10:31:43 2015 sebastien le-maire
** Last update Mon Aug 10 10:31:45 2015 sebastien le-maire
*/

#include "myc.h"

char		*my_strncpy(char *dest, const char *src,
			    const unsigned int size)
{
  unsigned int	i;

  i = 0;
  while ((dest[i] = src[i]) && i < size)
    ++i;
  dest[i] = '\0';
  return (dest);
}
