/*
** fct_write_fd.c for my_libc in /home/le-mai_s/librairie/librairie_C/my_libc/sources
**
** Made by sebastien le-maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Sun Aug 30 18:44:02 2015 sebastien le-maire
** Last update Sun Aug 30 19:03:53 2015 sebastien le-maire
*/

#include <unistd.h>
#include "myc.h"

void		my_fputc(const int fd, const char c)
{
  (void)write(fd, &c, 1);
}

void		my_fputs(const int fd, const char *str)
{
  (void)write(fd, str, my_strlen(str));
}

void		my_fputnbr(const int fd, int nb)
{
  if (nb < 0)
    {
      my_fputc(fd, '-');
      nb = -nb;
    }
  if (nb >= 10)
    {
      my_fputnbr(fd, nb / 10);
      my_fputnbr(fd, nb % 10);
    }
  else
    my_fputc(fd, INT_TO_CHAR(nb));
}

void		my_fputnbr_base(const int fd, int nbr, const char *base)
{
  unsigned int	size_base;
  unsigned int	divisor;
  unsigned int	nb;

  divisor = 1;
  size_base = my_strlen(base);
  if (nbr < 0)
    {
      my_fputc(fd, '-');
      nbr *= -1;
    }
  while ((nbr / divisor) >= size_base)
    divisor *= size_base;
  while (divisor >= size_base)
    {
      nb = (nbr - (nbr % divisor)) / divisor;
      my_fputc(fd, base[nb]);
      nbr %= divisor;
      divisor /= size_base;
    }
  my_fputc(fd, base[nbr]);
}
