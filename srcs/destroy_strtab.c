/*
** destroy_strtab.c for libc in /home/le-mai_s/librairie/librairie_C/my_libc
**
** Made by sebastien le-maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Tue Aug 11 08:59:03 2015 sebastien le-maire
** Last update Tue Aug 11 09:00:30 2015 sebastien le-maire
*/

#include <stdlib.h>
#include "myc.h"

void		destroy_strtab(char **tab)
{
  unsigned int	y;

  if (!tab)
    return ;
  y = 0;
  while (tab[y])
    free(tab[y++]);
  free(tab);
  tab = NULL;
}
