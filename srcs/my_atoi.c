/*
** my_atoi.c for libc in /home/le-mai_s/librairie/librairie_C/libc
**
** Made by sebastien le-maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Mon Aug 10 10:31:04 2015 sebastien le-maire
** Last update Sat Oct 24 11:34:55 2015 sebastien le-maire
*/

#include "myc.h"

int		my_atoi(const char *str)
{
  int		nb;
  int		s;
  unsigned int	pos;

  s = (str[0] == '-') ? -1 : 1;
  pos = (s == -1) ? 1 : 0;
  nb = 0;
  while (str[pos] && IS_NUM(str[pos]))
    {
      nb = (nb * 10) + CHAR_TO_INT(str[pos]);
      ++pos;
    }
  return (nb * s);
}
