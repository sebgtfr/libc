/*
** my_putchar.c for libc in /home/le-mai_s/librairie/librairie_C/libc
**
** Made by sebastien le-maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Mon Aug 10 10:30:04 2015 sebastien le-maire
** Last update Fri Sep 18 10:46:28 2015 sebastien le-maire
*/

#include <unistd.h>
#include "myc.h"

void		my_putchar(const char c)
{
  (void)write(STDOUT_FILENO, &c, 1);
}
