/*
** newstr.c for libc in /home/le-mai_s/librairie/librairie_C/libc
**
** Made by sebastien le-maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Mon Aug 10 10:33:18 2015 sebastien le-maire
** Last update Mon Aug 10 10:33:19 2015 sebastien le-maire
*/

#include <stdlib.h>
#include "myc.h"

char		*newstr(const unsigned int size)
{
  char		*new;
  unsigned int	pos;

  if (!(new = malloc(sizeof(char) * (size + 1))))
    return (NULL);
  pos = 0;
  while (pos <= size)
    new[pos++] = '\0';
  return (new);
}
