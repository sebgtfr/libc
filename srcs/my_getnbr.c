/*
** my_getnbr.c for libc in /home/le-mai_s/librairie/librairie_C/libc
**
** Made by sebastien le-maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Mon Aug 10 10:31:17 2015 sebastien le-maire
** Last update Mon Sep 21 15:02:18 2015 sebastien le-maire
*/

#include <limits.h>
#include "myc.h"

/*
** count_sign is bool for activate count of sign '-' for the sign
*/
int		my_getnbr(const char *str, t_bool count_sign)
{
  int		nb;
  int		prev;
  int		s;
  unsigned int	pos;
  unsigned int	cpt;

  pos = 0;
  cpt = 0;
  while (str[pos] && NOT_NUM(str[pos]))
    cpt += (str[pos++] == '-') ? 1 : 0;
  if (count_sign == TRUE)
    s = ((cpt % 2) == 1) ? -1 : 1;
  else
    s = (pos > 0 && str[pos - 1] == '-') ? -1 : 1;
  nb = 0;
  prev = 0;
  while (str[pos] && IS_NUM(str[pos]) && nb >= prev)
    {
      prev = nb;
      nb = (nb * 10) + CHAR_TO_INT(str[pos++]);
    }
  return (nb * s);
}
