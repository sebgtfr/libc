/*
** my_strncmp.c for libc in /home/le-mai_s/librairie/librairie_C/libc
**
** Made by sebastien le-maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Mon Aug 10 10:32:27 2015 sebastien le-maire
** Last update Thu Oct  6 18:17:17 2016 Sébastien Le Maire
*/

#include "myc.h"

int		my_strncmp(const char *s1, const char *s2,
			   const unsigned int size)
{
  unsigned int	pos;

  pos = 0;
  while ((s1[pos] || s2[pos]) && (pos < size) && (s1[pos] == s2[pos]))
    ++pos;
  if (pos == size)
    --(pos);
  return (s1[pos] - s2[pos]);
}
