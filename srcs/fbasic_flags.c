/*
** fbasic_flags.c for libc in /home/le-mai_s/librairie/librairie_C/my_libc/sources
**
** Made by sebastien le-maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Sun Aug 30 18:53:22 2015 sebastien le-maire
** Last update Sun Aug 30 18:55:11 2015 sebastien le-maire
*/

#include "my_lib_c.h"

void		fd_flag_char(const int fd, va_list *ap)
{
  my_fputc(fd, (char)va_arg(*ap, int));
}

void		fd_flag_string(const int fd, va_list *ap)
{
  my_fputs(fd, va_arg(*ap, char *));
}

void		fd_flag_entier(const int fd, va_list *ap)
{
  my_fputnbr(fd, va_arg(*ap, int));
}

void		fd_flag_hexa(const int fd, va_list *ap)
{
  my_fputnbr_base(fd, va_arg(*ap, int), "0123456789ABCDEF");
}

void		fd_flag_binaire(const int fd, va_list *ap)
{
  my_fputnbr_base(fd, va_arg(*ap, int), "01");
}
