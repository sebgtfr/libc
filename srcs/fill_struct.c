/*
** fill_struct.c for my_libc in /home/le-mai_s/librairie/librairie_C/my_libc/sources
**
** Made by sebastien le-maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Thu Nov  5 19:06:51 2015 sebastien le-maire
** Last update Thu Nov  5 19:14:51 2015 sebastien le-maire
*/

#include <stdlib.h>
#include "my_lib_c.h"

void		*fill_struct(void *structure, void *(*fill_data)(),
			     const unsigned int size, ...)
{
  void		*new;
  va_list	ap;

  if (!(new = (size) ? malloc(size) : structure))
    return (NULL);
  va_start(ap, size);
  if (!(new = fill_data(new, &ap)))
    return (NULL);
  va_end(ap);
  return (new);
}
