/*
** my_strncat.c for libc in /home/le-mai_s/librairie/librairie_C/libc
**
** Made by sebastien le-maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Mon Aug 10 10:32:05 2015 sebastien le-maire
** Last update Mon Aug 10 10:32:06 2015 sebastien le-maire
*/

#include "myc.h"

char	*my_strncat(char *dest, const char *src, const unsigned int size)
{
  unsigned int	i;
  unsigned int	j;

  i = 0;
  j = my_strlen(dest);
  while (src[i] && i < size)
    {
      dest[j] = src[i];
      ++i;
      ++j;
    }
  dest[j] = '\0';
  return (dest);
}
