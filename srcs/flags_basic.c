#include "my_lib_c.h"

void		flag_char(va_list *ap)
{
  my_putchar((char)va_arg(*ap, int));
}

void		flag_string(va_list *ap)
{
  my_putstr(va_arg(*ap, char *));
}

void		flag_entier(va_list *ap)
{
  my_putnbr(va_arg(*ap, int));
}

void		flag_hexa(va_list *ap)
{
  my_putnbr_base(va_arg(*ap, int), "0123456789ABCDEF");
}

void		flag_binaire(va_list *ap)
{
  my_putnbr_base(va_arg(*ap, int), "01");
}
