/*
** my_fprintf.c for libc in /home/le-mai_s/librairie/librairie_C/my_libc/sources
**
** Made by sebastien le-maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Sun Aug 30 18:49:35 2015 sebastien le-maire
** Last update Wed Oct 21 10:23:11 2015 sebastien le-maire
*/

#include "my_lib_c.h"

static void    	finit_flags(t_fd_flag (*flag)[NB_FLAGS])
{
  (*flag)[0].c = 'c';
  (*flag)[0].fct = fd_flag_char;
  (*flag)[1].c = 's';
  (*flag)[1].fct = fd_flag_string;
  (*flag)[2].c = 'd';
  (*flag)[2].fct = fd_flag_entier;
  (*flag)[3].c = 'i';
  (*flag)[3].fct = fd_flag_entier;
  (*flag)[4].c = 'h';
  (*flag)[4].fct = fd_flag_hexa;
  (*flag)[5].c = 'b';
  (*flag)[5].fct = fd_flag_binaire;
}

static int    	fcheck_flags(const char c, va_list *ap,
			     t_fd_flag *flag, const int fd)
{
  unsigned int	i;

  i = 0;
  while (i < NB_FLAGS && flag[i].c != c)
    ++i;
  if (i == NB_FLAGS)
    return (1);
  flag[i].fct(fd, ap);
  return (0);
}

void		my_fprintf(const int fd, const char *format, ...)
{
  va_list	ap;
  t_fd_flag	flag[NB_FLAGS];
  unsigned int	i;

  i = 0;
  finit_flags(&flag);
  va_start(ap, format);
  while (format[i])
    {
      if (format[i] == '%')
	{
	  if (fcheck_flags(format[i + 1], &ap, flag, fd))
	    my_fputc(fd, format[i]);
	  else
	    ++i;
	}
      else
	my_fputc(fd, format[i]);
      ++i;
    }
  va_end(ap);
}
